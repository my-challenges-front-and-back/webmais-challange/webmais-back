/*
  Warnings:

  - Made the column `weight` on table `Products` required. This step will fail if there are existing NULL values in that column.
  - Made the column `length` on table `Products` required. This step will fail if there are existing NULL values in that column.
  - Made the column `width` on table `Products` required. This step will fail if there are existing NULL values in that column.
  - Made the column `height` on table `Products` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Products" ALTER COLUMN "weight" SET NOT NULL,
ALTER COLUMN "length" SET NOT NULL,
ALTER COLUMN "width" SET NOT NULL,
ALTER COLUMN "height" SET NOT NULL,
ALTER COLUMN "code" DROP NOT NULL;
