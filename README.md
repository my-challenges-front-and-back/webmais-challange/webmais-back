# <p align = "center"> WebMais Sistemas </p>

<div style="background: #eddfbd; padding: 30px">
  <p align="center" >
  <img src="./logo-webmais.svg" width="150"/>
</p>

</div>

<br />

## :computer: Technologies

- TypeScript
- Node.js
- Eslint, Prettier
- CommitLint
- Husky
- Prisma
- Joi
- Docker

---

## Description

<p>
proposed challenge to make an improvement in the product register where the user can enter the code manually and the width, height and length of the product
</p>

<br />

## 🏁 running the application with docker

<h3 style="color:#ef4444; font-weight: bolder">Notice</h3>

<br />

<p>
  To run the step below to run the application you must have docker and docker-compose installed on your machine you can get more information here <a href="https://docs.docker.com/engine/install/">install docker.</a>
</p>
<br />


## 1 - First Step

```
  - Clone the repository: https://gitlab.com/my-challenges-front-and-back/webmais-challange/webmais-back.git
```



## 2 - Second Step

```
- Navigate into the cloned repository
```


## 3 - Third step

```
  yarn install or npm install
```

## 4 - Fourth step 
```
docker-compose up -d
```

## 5 - Fifth step

```
  yarn dev or npm run dev
```

## :rocket: Router Product


```yml
POST /product
    - route to add new Product
    - headers: {}
    - body: {
        title: "My Product",
        description: "First product",
        code: "4234234",
        price: 29.90,
        width: 10.2,
        height: 3.2,
        length: 7.3,
        comments: [
          {"comment": "first comment"},
          {"comment": "second comment"}
        ]
    }
```

## 📄 To view more detailed application documentation visit the url

```json
http:localhost:3001/docs
```

## 🧪🧪 Running tests with jest
```
  npm run test or yarn test
````