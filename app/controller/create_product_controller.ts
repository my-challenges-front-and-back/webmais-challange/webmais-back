import { Request, Response } from 'express'
import { ProductAdd } from '../domain/product'

export class CreateProductController {
  constructor(private productUseCase: ProductAdd) {}

  async handle(req: Request, res: Response) {
    await this.productUseCase.execute(req.body)
    res.sendStatus(201)
  }
}
