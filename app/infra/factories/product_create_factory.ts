import { CreateProductController } from '../../controller/create_product_controller'
import { CreateProductUseCase } from '../../usecase/create_product'
import { ProductRepositorie } from '../repositories/product_repositorie'
import { GenerateCode } from '../utils/generate_code'

const productRepository = new ProductRepositorie()
const generateCodeProduct = new GenerateCode()
const productUseCase = new CreateProductUseCase(
  productRepository,
  generateCodeProduct
)
const productControllerFactory = new CreateProductController(productUseCase)

export default productControllerFactory
