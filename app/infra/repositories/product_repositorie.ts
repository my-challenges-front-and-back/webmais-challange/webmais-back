import { ProductProperty } from '../../domain/product'
import { IProductRepository } from '../../usecase/create_product_repository'
import prisma from '../config/prisma_connection'
import { formatResponse } from '../utils/adapter_response'

export class ProductRepositorie implements IProductRepository {
  async findByCode(code: string): Promise<ProductProperty> {
    const productExist = await prisma.products.findUnique({
      where: {
        code
      }
    })
    const responseFormated = formatResponse(productExist)
    return responseFormated
  }

  async SaveProduct(product: ProductProperty): Promise<void> {
    await prisma.products.create({
      data: {
        title: product.title,
        description: product?.description,
        price: product?.price,
        code: product?.code,
        width: product.width,
        height: product.height,
        length: product.length,
        Comments: {
          createMany: {
            data: product?.comments?.length > 0 ? product.comments : []
          }
        }
      }
    })
  }
}
