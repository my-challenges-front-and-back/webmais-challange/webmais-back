import { Router } from 'express'
import productControllerFactory from '../factories/product_create_factory'

/* middleware */
import validation from '../middlewares/validation'

/* product validation */
import productValidation from '../validation/product_validation'

const productRouter = Router()

productRouter.post('/product', validation(productValidation), (req, res) =>
  productControllerFactory.handle(req, res)
)

export default productRouter
