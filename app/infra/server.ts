import app from './config/app'

app.listen(process.env.PORT || 3001, () =>
  console.log(`app running in port ${process.env.PORT} 🚀🚀`)
)
