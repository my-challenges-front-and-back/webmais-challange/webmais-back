import { Products } from '@prisma/client'
import { ProductProperty } from '../../domain/product'

export const formatResponse = (data: Products) => {
  const product: ProductProperty = {
    id: data?.id,
    title: data?.title,
    description: data?.description,
    price: Number(data?.price),
    code: data?.code,
    width: data?.width,
    height: data?.height,
    length: data?.length
  }
  return product
}
