import { IGenerateCode } from '../../usecase/generate_code_product'
import shortId from 'shortid'

export class GenerateCode implements IGenerateCode {
  random(): string {
    return shortId.generate()
  }
}
