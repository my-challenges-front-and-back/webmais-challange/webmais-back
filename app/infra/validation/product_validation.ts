import * as Joi from 'joi'

const productValidation = Joi.object({
  title: Joi.string().required().messages({
    'string.empty': 'Título é obrigatório',
    'any.required': 'Por favor informe o titulo'
  }),
  description: Joi.string().allow(''),
  price: Joi.number(),
  code: Joi.string().allow(''),
  width: Joi.number().required().messages({
    'number.base': 'Largura deve ser um número.',
    'any.required': 'Por favor, informe a largura.'
  }),
  height: Joi.number().required().messages({
    'number.base': 'Altura deve ser um número',
    'any.required': 'Por favor, informe a altura'
  }),
  length: Joi.number().required().messages({
    'number.base': 'Comprimento deve ser um número',
    'any.required': 'Por favor, informe o comprimento.'
  }),
  comments: Joi.array()
    .items(Joi.object({ comment: Joi.string().required() }))
    .messages({
      'array.base': 'A lista deve ser um array',
      'string.base': 'O item da lista deve ser do tipo string'
    })
})

export default productValidation
