import 'express-async-errors'
import 'dotenv/config'
import cors from 'cors'
import express, { json } from 'express'

/*routers */
import productRouter from '../router/product_router'

/* middleware */
import error from '../middlewares/error'

/* swagger */
import swaggerDoc from 'swagger-ui-express'
import swaggerRules from '../../../swagger.json'

const app = express()

app.use(json())
app.use(cors())

/* routers */
app.use(productRouter)
app.use('/docs', swaggerDoc.serve, swaggerDoc.setup(swaggerRules))

/* middleware */
app.use(error)

export default app
