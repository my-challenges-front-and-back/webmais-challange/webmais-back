import { ProductAdd, ProductProperty } from '../domain/product'
import { ErrorConfict } from '../error/error_conflict'
import { IProductRepository } from './create_product_repository'
import { IGenerateCode } from './generate_code_product'

export class CreateProductUseCase implements ProductAdd {
  constructor(
    private productRepository: IProductRepository,
    private generateCode: IGenerateCode
  ) {}

  async execute(productProperty: ProductProperty): Promise<void> {
    const codeWasSent =
      productProperty?.code && productProperty?.code?.length > 0
        ? productProperty.code
        : this.generateCode.random()

    productProperty.code = codeWasSent

    const foundCode = await this.productRepository.findByCode(codeWasSent)

    if (foundCode?.code) {
      throw new ErrorConfict('O código do produto já existe', 409)
    }

    await this.productRepository.SaveProduct(productProperty)
  }
}
