import { ProductProperty } from '../domain/product'

export interface IProductRepository {
  findByCode(ean: string): Promise<ProductProperty | null>
  SaveProduct(product: ProductProperty): Promise<void>
}
