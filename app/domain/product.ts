interface Comment {
  comment: string
}

export interface ProductProperty {
  id: string
  title: string
  description?: string
  price?: number
  code?: string
  length: number
  width: number
  height: number
  comments?: Array<Comment>
}

export interface ProductAdd {
  execute(productProperty: ProductProperty): Promise<void>
}
