/*
 * For a detailed explanation regarding each configuration property and type check, visit:
 * https://jestjs.io/docs/configuration
 */

export default {
  clearMocks: true,
  collectCoverageFrom: ['<rootDir>/app/**/*.ts'],
  collectCoverage: false,
  coverageDirectory: 'coverage',
  coverageProvider: 'babel',
  // roots: ['<rootDir>/tests', '<rootDir>/app'],
  transform: {
    '\\.ts$': 'ts-jest'
  }
}
