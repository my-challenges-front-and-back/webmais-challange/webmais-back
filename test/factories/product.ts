import prisma from '../../app/infra/config/prisma_connection'

export const addProduct = async () => {
  await prisma.products.create({
    data: {
      title: 'mock product',
      code: 'mock-code',
      height: 2.0,
      width: 2.0,
      length: 2.0
    }
  })
}

export const clearProducts = async () => {
  await prisma.products.deleteMany()
}
