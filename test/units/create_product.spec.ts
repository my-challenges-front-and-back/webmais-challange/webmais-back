import { ProductProperty } from '../../app/domain/product'
import { IProductRepository } from '../../app/usecase/create_product_repository'
import { CreateProductUseCase } from '../../app/usecase/create_product'
import { IGenerateCode } from '../../app/usecase/generate_code_product'

const productResponse = {
  id: '123',
  title: 'Produto exemplo',
  description: 'Descrição do produto',
  price: 9.99,
  code: '7890123456789',
  weight: 0.5,
  length: 10,
  width: 5,
  height: 2
}

const productRequest = {
  id: 'any-id',
  title: 'any-title',
  code: 'any-ean',
  weight: 0.5,
  length: 0.5,
  width: 0.5,
  height: 0.5
}

class GenerateCodeMock implements IGenerateCode {
  random(): string {
    return `${Math.floor(Math.random() * 2000000)}`
  }
}

class ProductRepositoryMock implements IProductRepository {
  public products: Array<ProductProperty> = []

  async SaveProduct(product: ProductProperty): Promise<void> {
    await this.products.push(product)
  }
  async findByCode(code: string): Promise<ProductProperty | null> {
    return new Promise((resolved) => resolved(productResponse))
  }
}

let productRepository: ProductRepositoryMock
let productUseCase: CreateProductUseCase

describe('Product', () => {
  beforeAll(() => {
    productRepository = new ProductRepositoryMock()
    const codeProductGenerate = new GenerateCodeMock()
    productUseCase = new CreateProductUseCase(
      productRepository,
      codeProductGenerate
    )
  })

  it('should return an error if the product ean exists', async () => {
    const response = productUseCase.execute(productRequest)
    await expect(response).rejects.toThrow('O código do produto já existe')
  })
  it('should call method execute with correct params', async () => {
    jest.spyOn(productRepository, 'findByCode').mockReturnValue(null)
    const spyExecuteMethod = jest.spyOn(productUseCase, 'execute')
    productUseCase.execute(productRequest)
    expect(spyExecuteMethod).toHaveBeenCalledWith(productRequest)
  })
  it('should register a product correctly if the mandatory parameters are provided', () => {
    jest.spyOn(productRepository, 'findByCode').mockReturnValue(null)
    productUseCase.execute(productRequest)
    expect(productRepository.products.length).toBe(1)
  })
})
