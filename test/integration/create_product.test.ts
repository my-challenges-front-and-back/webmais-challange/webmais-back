import supertest from 'supertest'
import app from '../../app/infra/config/app'
import { addProduct, clearProducts } from '../factories/product'

describe('Product', () => {
  beforeAll(async () => {
    await addProduct()
  })
  afterAll(async () => {
    await clearProducts()
  })
  it('should get an error if the product title is empty', async () => {
    const response = await supertest(app).post('/product').send({
      title: '',
      width: 0.3,
      height: 0.3,
      length: 0.4
    })
    expect(response.status).toBe(422)
    expect(response.body).toEqual({ error: `Título é obrigatório` })
  })
  it('should get an error if the product title is not provided', async () => {
    const response = await supertest(app).post('/product').send({
      width: 0.3,
      height: 0.3,
      length: 0.4
    })
    expect(response.status).toBe(422)
    expect(response.body).toEqual({ error: `Por favor informe o titulo` })
  })
  it('should get an error if the product width is not provided', async () => {
    const response = await supertest(app).post('/product').send({
      title: 'First product',
      height: 0.3,
      length: 0.4
    })
    expect(response.status).toBe(422)
    expect(response.body).toEqual({ error: `Por favor, informe a largura.` })
  })
  it('should get an error if the product height is not provided', async () => {
    const response = await supertest(app).post('/product').send({
      title: 'First product',
      width: 0.3,
      length: 0.4
    })
    expect(response.status).toBe(422)
    expect(response.body).toEqual({ error: `Por favor, informe a altura` })
  })
  it('should get an error if the product length is not provided', async () => {
    const response = await supertest(app).post('/product').send({
      title: 'First product',
      width: 0.3,
      height: 2.1
    })
    expect(response.status).toBe(422)
    expect(response.body).toEqual({
      error: `Por favor, informe o comprimento.`
    })
  })
  it('should register a product if all parameters are correct', async () => {
    const response = await supertest(app).post('/product').send({
      title: 'First product',
      width: 0.3,
      height: 2.1,
      length: 0.4
    })
    expect(response.status).toBe(201)
  })
  it('should receive an error if the product code is already registered', async () => {
    const response = await supertest(app).post('/product').send({
      title: 'First product',
      code: 'mock-code',
      width: 0.3,
      height: 2.1,
      length: 0.4
    })
    expect(response.status).toBe(409)
    expect(response.body).toEqual({ error: 'O código do produto já existe' })
  })
})
